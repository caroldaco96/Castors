class Box(object):
	def __init__(self):
		self._contains=[]
		self._open=True

	def add(self,objet):
		self._contains.append(objet)

	def objet(self):
		return self._contains

	def affiche(self):
		print("Votre boite contient: ", end='')
		for objet in self.objet():
			print(objet, ' ', end='')

	def __contains__(self, objet):
		return objet in self._contains

	def remove(self, objet):
		if self.__contains__(objet):
			self._contains.remove(objet)
			return True
		else:
			return False

	def is_open(self):
		return self._open

	def close(self):
		self._open=False

	def open(self):
		self._open=True
		
