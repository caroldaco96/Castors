from tp2 import *

def test_box_create():
    b = Box(50)
    b.add_room_for(10,"truc1")
    b.add_room_for(10,"truc2")
    assert "truc1" in b
    assert "truc2" in b 
    assert "truc3" not in b
    b.close()
    assert (b.is_open() == False)
    b.open()
    assert (b.is_open() == True)
    assert(b.action_look() == 'La boite contient : truc1 truc2 ')
    
def test_volume():
    c = Objet(3)
    assert (c.get_volume() == 3)
    c.set_volume(4)
    assert (c.get_volume() == 4)
    b = Box()
    assert (b.get_capacity() == None)
    b.set_capacity(10)
    assert(b.get_capacity() == 10)
    b.set_capacity(20)
    assert(b.get_capacity() == 20)
    b.add_room_for(10,"Hallo")
    b.add_room_for(10,"Hallo")
    assert(b.action_look() == 'La boite contient : Hallo Hallo ')
    b.close()
    assert(b.action_look() == 'La boite est fermée')
    assert(b.set_name('Hallo') == 'Hallo')
    assert(b.set_name('o') == None)
    
    
    
